package com.enigma.excangeapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.enigma.excangeapp.model.GetRateResponse
import com.enigma.excangeapp.model.RateBindingModel
import com.enigma.excangeapp.network.RatesRepository
import com.enigma.excangeapp.utils.getCurrencyNameForCode
import com.enigma.excangeapp.utils.getFlagIdForCurrencyCode
import io.reactivex.functions.Consumer
import java.util.*

class MainViewModel : ViewModel() {


    private val liveData: MutableLiveData<LinkedList<RateBindingModel>> = MutableLiveData()
    private var amount: Double = 1.0
    private var isInitialized: Boolean = false

    fun observer(): LiveData<LinkedList<RateBindingModel>> {
        if (!isInitialized) {
            isInitialized = true
            getRates("EUR")
        }
        return liveData
    }

    override fun onCleared() {
        super.onCleared()
        RatesRepository.clear()
    }

    fun getRates(code: String) {
        RatesRepository.getRates(code, onNewRates)
    }

    private val onNewRates: Consumer<GetRateResponse> = Consumer {
        liveData.value = updateList(it)
    }

    fun onItemClicked(position: Int) {
        liveData.value?.let {
            it.addFirst(it.removeAt(position))
            amount = java.lang.Double.parseDouble(it[0].value.replace(",","."))
            it[0].rate = 1.0
            getRates(it[0].code)
        }
    }

    fun onValueChanged(value: Double, position: Int) {
        if (position != 0) onItemClicked(position)
        amount = value
        liveData.value?.forEach { rbm: RateBindingModel -> rbm.value = getValue(rbm.rate) }
        liveData.postValue(liveData.value)
    }

    private fun updateList(rateResponse: GetRateResponse): LinkedList<RateBindingModel> {
        val list = liveData.value?: LinkedList()
        list.takeIf { it.isEmpty() }?.let {
            list.add(
                RateBindingModel(
                    rateResponse.baseCurrency,
                    getCurrencyNameForCode(rateResponse.baseCurrency),
                    getFlagIdForCurrencyCode(rateResponse.baseCurrency),
                    1.0, "1.00"))
            for ((k, v) in rateResponse.rates) {
                list.add(RateBindingModel(k, getCurrencyNameForCode(k), getFlagIdForCurrencyCode(k), v, getValue(v)))
            }
        } ?: list.let {
            for (i in 1 until list.size)
                list[i].let {
                    it.rate = rateResponse.rates.getOrElse(it.code) { 1.0 }
                    it.value = getValue(it.rate)
                }
        }
        return list
    }

    fun getValue(rate: Double): String = "%.2f".format(rate * amount)
}
