package com.enigma.excangeapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.enigma.excangeapp.R
import com.enigma.excangeapp.adapters.ExchangeAdapter
import kotlinx.android.synthetic.main.main_fragment.view.*
import java.util.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        view!!.list.adapter = ExchangeAdapter(LinkedList(), viewModel)
        viewModel.observer().observe(activity!!, Observer { (view!!.list.adapter!! as ExchangeAdapter).updateList(it) })
    }

}
