package com.enigma.excangeapp.model

import androidx.annotation.DrawableRes

data class RateBindingModel(
    val code: String,
    val name: String, @DrawableRes val icon: Int,
    var rate: Double,
    var value: String
)