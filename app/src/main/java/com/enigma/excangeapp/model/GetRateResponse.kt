package com.enigma.excangeapp.model

import com.google.gson.annotations.SerializedName

data class GetRateResponse(
    @SerializedName("baseCurrency") val baseCurrency: String,
    @SerializedName("rates") val rates: Map<String, Double>)