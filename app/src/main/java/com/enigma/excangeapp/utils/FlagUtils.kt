package com.enigma.excangeapp.utils

import androidx.annotation.DrawableRes
import com.enigma.excangeapp.R
import java.util.*

@DrawableRes
fun getFlagIdForCurrencyCode(code: String): Int = when (code.toLowerCase(Locale.ROOT)) {
    "eur" -> R.drawable.eur
    "aud" -> R.drawable.aud
    "bgn" -> R.drawable.bgn
    "brl" -> R.drawable.brl
    "cad" -> R.drawable.cad
    "chf" -> R.drawable.chf
    "cny" -> R.drawable.cny
    "czk" -> R.drawable.czk
    "dkk" -> R.drawable.dkk
    "gbp" -> R.drawable.gbp
    "hkd" -> R.drawable.hkd
    "hrk" -> R.drawable.hrk
    "huf" -> R.drawable.huf
    "idr" -> R.drawable.idr
    "ils" -> R.drawable.ils
    "inr" -> R.drawable.inr
    "isk" -> R.drawable.isk
    "jpy" -> R.drawable.jpy
    "krw" -> R.drawable.krw
    "mxn" -> R.drawable.mxn
    "myr" -> R.drawable.myr
    "nok" -> R.drawable.nok
    "nzd" -> R.drawable.nzd
    "php" -> R.drawable.php
    "pln" -> R.drawable.pln
    "ron" -> R.drawable.ron
    "rub" -> R.drawable.rub
    "sek" -> R.drawable.sek
    "sgd" -> R.drawable.sgd
    "thb" -> R.drawable.thb
    "usd" -> R.drawable.usd
    "zar" -> R.drawable.zar
    else -> R.drawable.flag
}