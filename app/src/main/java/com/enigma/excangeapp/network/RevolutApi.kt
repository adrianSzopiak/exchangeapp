package com.enigma.excangeapp.network

import com.enigma.excangeapp.model.GetRateResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutApi {

    @GET("api/android/latest")
    fun getRateList(@Query("base") base: String): Observable<GetRateResponse>
}