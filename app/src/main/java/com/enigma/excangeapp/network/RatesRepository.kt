package com.enigma.excangeapp.network

import com.enigma.excangeapp.model.GetRateResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


object RatesRepository {

    private val revolutApi: RevolutApi = RetrofitService.createService(RevolutApi::class.java)
    private var disposable: Disposable? = null

    fun clear() {
        disposable?.dispose()
    }

    fun getRates(base: String, consumer: Consumer<GetRateResponse>) {
        clear()
        disposable = Observable.interval(0, 1000, TimeUnit.MILLISECONDS).doOnEach {
            revolutApi.getRateList(base)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer)
        }.subscribe()
    }
}