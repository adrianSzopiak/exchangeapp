package com.enigma.excangeapp.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("image")
fun getImage(view: ImageView, id: Int) {
    view.setImageResource(id)
}
