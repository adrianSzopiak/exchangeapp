package com.enigma.excangeapp.adapters

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.inflate
import androidx.recyclerview.widget.RecyclerView
import com.enigma.excangeapp.R
import com.enigma.excangeapp.databinding.RateListItemBinding
import com.enigma.excangeapp.model.RateBindingModel
import com.enigma.excangeapp.ui.main.MainViewModel
import com.enigma.excangeapp.utils.DecimalDigitsInputFilter
import kotlinx.android.synthetic.main.rate_list_item.view.*
import java.util.*


class ExchangeAdapter(var rates: LinkedList<RateBindingModel>, val viewModel: MainViewModel) :
    RecyclerView.Adapter<ExchangeAdapter.ViewHolder>() {

    var recyclerView: RecyclerView? = null

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            inflate(
                LayoutInflater.from(parent.context),
                R.layout.rate_list_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(rates[position])
        holder.itemView.setOnClickListener { onItemClick(holder) }
        holder.itemView.editText.setOnClickListener { onItemClick(holder) }
        holder.itemView.editText.setOnFocusChangeListener {_: View?, focused: Boolean -> if (focused) onItemClick(holder) }
        if (position == 0) {
            holder.itemView.editText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (holder.itemView.editText.hasFocus()) {
                        val field: Double = p0.takeIf { !it.isNullOrEmpty() }?.let { java.lang.Double.parseDouble(p0.toString().replace(",", ".")) } ?: 0.0
                        viewModel.onValueChanged(field, holder.adapterPosition)
                    }
                }
            })}
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) super.onBindViewHolder(holder, position, payloads)
        else {
            if (payloads.any {it is RateChanged}) {
                holder.itemView.editText.setText(rates[position].value)
            }
        }
    }

    override fun getItemCount(): Int = rates.size

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun onItemClick(holder: ViewHolder) {
        if (holder.adapterPosition == 0) return
        rates.addFirst(rates.removeAt(holder.adapterPosition))
        viewModel.onItemClicked(holder.adapterPosition)
        notifyItemMoved(holder.adapterPosition, 0)
        notifyDataSetChanged()
        recyclerView?.smoothScrollToPosition(0)
    }

    fun updateList(list: List<RateBindingModel>) {
        if (rates.isEmpty()) {
            rates.addAll(list)
            notifyDataSetChanged()
        } else {
            for (i in 1 until rates.size) {
                notifyItemChanged(i, RateChanged())
            }
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        this.recyclerView = null
    }

    class ViewHolder(private val binding: RateListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(bindingModel: RateBindingModel) {
            with(binding) {
                model = bindingModel
            }
            binding.editText.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter())
        }


    }

    class RateChanged
}